class CreatePrints < ActiveRecord::Migration
  def change
    create_table :prints do |t|
        t.string :name
        t.string 'image_uid'
        t.timestamps null: false
    end
  end
end
