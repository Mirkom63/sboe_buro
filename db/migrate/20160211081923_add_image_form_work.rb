class AddImageFormWork < ActiveRecord::Migration
  def change 
      remove_column :works, :logo, :string
      add_column :works, :logo_uid, :string
      add_column :works, :marker_uid, :string
  end
end
