class CreateAddRefImageToNews < ActiveRecord::Migration
  def change
    create_table :add_ref_image_to_news do |t|
        add_reference :images, :news, index: true
      t.timestamps null: false
    end
  end
end
