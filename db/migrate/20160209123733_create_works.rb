class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
        t.string :name
        t.string :logo
        t.string :coordinate 
      t.timestamps null: false
    end
  end
end
