class AddPosition < ActiveRecord::Migration
  def change
      add_column :news, :position, :integer
      remove_column :news, :ancestry
  end
end
