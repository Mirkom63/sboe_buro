class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
        t.string :name
        t.string :description
        t.string :youtube
        t.string 'image_uid'
        t.timestamps null: false
    end
  end
end
