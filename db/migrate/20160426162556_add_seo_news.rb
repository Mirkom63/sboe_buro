class AddSeoNews < ActiveRecord::Migration
  def change
      add_column :news, :description, :text
      add_column :news, :keywords, :text
  end
end
