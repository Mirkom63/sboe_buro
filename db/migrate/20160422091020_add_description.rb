class AddDescription < ActiveRecord::Migration
  def change
      add_column :news, :detail, :text
      add_column :news, :preview, :text
  end
end
