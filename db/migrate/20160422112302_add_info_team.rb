class AddInfoTeam < ActiveRecord::Migration
  def change
      add_column :teams, :occupation, :string
      add_column :teams, :arrange, :string
      add_column :teams, :position, :integer
  end
end
