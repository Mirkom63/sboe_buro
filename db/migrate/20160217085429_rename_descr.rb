class RenameDescr < ActiveRecord::Migration
  def change
      remove_column :news, :description
      add_column :news, :description, :text
  end
end
