class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
        t.references :work, index: true, foreign_key: true
        t.string 'src_uid'
        t.timestamps null: false
    end
  end
end
