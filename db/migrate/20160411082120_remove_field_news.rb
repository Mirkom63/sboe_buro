class RemoveFieldNews < ActiveRecord::Migration
  def change
      remove_column :news, :youtube
      remove_column :news, :image_uid
      remove_column :news, :description
  end
end
