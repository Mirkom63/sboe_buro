class AddAltImage < ActiveRecord::Migration
  def change
      add_column :images, :alt, :string
      add_column :visuals, :alt, :string
  end
end
