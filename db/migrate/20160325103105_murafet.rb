class Murafet < ActiveRecord::Migration
  def change
      remove_column :images, :src_uid
      remove_column :visuals, :src_uid
      add_column :works, :panoram, :string
  end
end
