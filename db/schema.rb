# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160525105146) do

  create_table "add_ref_image_to_news", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "arts", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "image_uid",          limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.text     "detail",             limit: 65535
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
    t.string   "href",               limit: 255
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "news_id",    limit: 4
    t.string   "name",       limit: 255
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "comments", ["news_id"], name: "index_comments_on_news_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.integer  "work_id",            limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "news_id",            limit: 4
    t.integer  "position",           limit: 4
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "alt",                limit: 255
  end

  add_index "images", ["news_id"], name: "index_images_on_news_id", using: :btree
  add_index "images", ["work_id"], name: "index_images_on_work_id", using: :btree

  create_table "news", force: :cascade do |t|
    t.string   "name",                      limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "image_file_name",           limit: 255
    t.string   "image_content_type",        limit: 255
    t.integer  "image_file_size",           limit: 4
    t.datetime "image_updated_at"
    t.string   "detail_image_file_name",    limit: 255
    t.string   "detail_image_content_type", limit: 255
    t.integer  "detail_image_file_size",    limit: 4
    t.datetime "detail_image_updated_at"
    t.text     "detail",                    limit: 65535
    t.text     "preview",                   limit: 65535
    t.string   "video",                     limit: 255
    t.string   "href",                      limit: 255
    t.text     "description",               limit: 65535
    t.text     "keywords",                  limit: 65535
    t.integer  "position",                  limit: 4
    t.string   "ancestry",                  limit: 255
  end

  create_table "prints", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "image_uid",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "href",       limit: 255
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.text     "detail",              limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 4
    t.datetime "avatar_updated_at"
    t.string   "occupation",          limit: 255
    t.string   "arrange",             limit: 255
    t.integer  "position",            limit: 4
    t.integer  "position_mobile",     limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "visuals", force: :cascade do |t|
    t.integer  "work_id",            limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "position",           limit: 4
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.string   "alt",                limit: 255
  end

  add_index "visuals", ["work_id"], name: "index_visuals_on_work_id", using: :btree

  create_table "works", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "coordinate",  limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "href",        limit: 255
    t.string   "logo_uid",    limit: 255
    t.string   "marker_uid",  limit: 255
    t.string   "chaihona",    limit: 255
    t.integer  "position",    limit: 4
    t.string   "arrange",     limit: 255
    t.string   "panoram",     limit: 255
    t.boolean  "development"
    t.string   "name_href",   limit: 255
  end

  add_foreign_key "comments", "news"
  add_foreign_key "images", "works"
  add_foreign_key "visuals", "works"
end
