class SiteController < ApplicationController
    skip_before_filter :verify_authenticity_token


    def add_comment
        comment=Comment.new
        comment.name=params[:name]
        comment.message=params[:message]
        comment.news_id=params[:news]
        comment.save

        redirect_to :back
    end
    def load_art

        @art=Art.all.page(params[:id]).per(10).order(id: :desc)

        render 'load_art',layout: nil

    end

    def send_message

        message=''
        message+='Имя: '+params[:name]+'<br/>'
        message+='Телефон: '+params[:phone]+'<br/>'
        message+='E-mail: '+params[:email]+'<br/>'
        message+='Сообщение: '+params[:message]+'<br/>'

        Pony.mail(:to => 'svoeburo.pr@yandex.ru', :from => 'me@example.com', :subject => 'Заявка от svoeburo.com', :html_body => message)

        render plain: 'ok'
    end
    def return_coord
        @page=Work.find(params[:id])

        render plain: @page.coordinate
    end

    def return_id
        @page=Work.find_by(coordinate: params[:coord])

        if @page.marker.blank?
            image=''
        else
            image=@page.marker.url.to_s
        end

        call_back=@page.id.to_s+'|'+image

        render plain: call_back
    end
    def detail
        @page=Work.find_by(coordinate: params[:coord])
        render 'detail',layout:nil
    end

    def detail_mobile
        @page=Work.find(params[:id])
        render 'detail_mobile',layout:nil
    end

    def load_page
        if params[:pattern]=='catalog'
            @work=Work.all.order(position: :asc);
        end
        if params[:pattern]=='art'
            @art=Art.all
        end
        if params[:pattern]=='news'
            @news=News.all
            @print=Print.all
        end
        render params[:pattern], layout: nil
    end

    def mobile_device?
    if session[:mobile_override]
      session[:mobile_override] == "1"
    else
      (request.user_agent =~ /Mobile|webOS/) && (request.user_agent !~ /iPad/)
    end
  end
  helper_method :mobile_device?


    def index

        pattern=params[:pattern]

        if params[:pattern].blank?

            if mobile_device?
                pattern='index_mobile'
            else
                pattern='index'
            end


        end

        if params[:pattern]=='object'
            @work=Work.all.order(position: :asc);

            if mobile_device?
                pattern='object_mobile'
            else
                pattern='object'
            end

        end

        if params[:pattern]=='news'
            @page=News.find(params[:id])
            @comment=Comment.where(news_id: @page.id)
            @description=@page.description
            @keywords=@page.keywords
        end

        if params[:pattern]=='team'
            if mobile_device?
                @team=Team.all.order(position_mobile: :asc)
            else
                @team=Team.all.order(position: :asc)
            end

        end


        if params[:pattern]=='art'
            @art=Art.all.page(1).per(10).order(id: :desc)

            @image=Visual.all
            @image.each do |item|
                if item.image.blank?
                    item.image='http://svoeburo.com/system/dragonfly/development/'+item.src_uid.to_s
                    item.save
                end
            end
        end
        if params[:pattern]=='vse-svoe'
            @section=News.where(ancestry: nil)
            @news=News.all.order(position: :asc).page(params[:page]).per(12)
            @print=Print.all

            pattern='vse-svoe'

        end
        if params[:pattern]=='share_art'
            @image=Art.find(params[:id])
        end
        if params[:pattern]=='share_image'
            @image=Image.find(params[:id])
        end
        if params[:pattern]=='share_visual'
            @image=Visual.find(params[:id])
        end


        render pattern
    end


end
