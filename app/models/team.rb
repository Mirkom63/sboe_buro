class Team < ActiveRecord::Base
    
    
    has_attached_file :avatar
    validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png']
    
    
    rails_admin do
	   

        nestable_list true
       
        edit do
            field :name do
              label 'Название '
            end
            
            field :position_mobile do
              label 'Позиция мобилка '
            end
            
            field :avatar do
              label 'Аватар '
            end
            
            field :occupation do
              label 'Должность' 
            end
            
            field :detail, :ck_editor
            
            
        end 
        
         
    end
   
    
    
    
end
