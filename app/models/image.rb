class Image < ActiveRecord::Base
    belongs_to :work


    has_attached_file :image, styles: { mobile: "480x480"} 
    validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png']
 
end
