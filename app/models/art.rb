class Art < ActiveRecord::Base
    
    has_attached_file :photo, styles: { normal: "445x445>", big: '1920x1080' }
    validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
    

end
