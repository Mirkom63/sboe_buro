class Work < ActiveRecord::Base
    dragonfly_accessor :logo
    dragonfly_accessor :marker
    has_many :images, dependent: :destroy
    has_many :visuals, dependent: :destroy
    
    
    rails_admin do
	   

        nestable_list true
        
        
        
        edit do
            field :name do
              label 'Название '
            end
            field :coordinate do
              label 'Координаты '
            end
            
            field :href do
              label 'Ссылка '
            end
            
            field :name_href do
              label 'Название ссылки '
            end
            
            field :panoram do
              label 'Панорама '
            end
            
            field :images do
              label 'Фотографии '
            end
            
            field :visuals do
              label 'Визаулизации '
            end
            
            
        end 
        
        
    end
end
