class News < ActiveRecord::Base

    has_ancestry
    
    has_attached_file :image, styles: { normal: "280x280#" }
    validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png']
    
    
    has_attached_file :detail_image, styles: { normal: "900x900" }
    validates_attachment_content_type :detail_image, :content_type => ['image/jpeg', 'image/png']
    
    
    
    rails_admin do
	   

        nestable_list true

        nestable_tree true
        
        
        edit do
            field :name do
              label 'Название '
            end
            
            field :image do
              label 'Превью'
            end
            
            field :href do
              label 'Ссылка '
            end
            
            field :detail_image do
              label 'Детальная '
            end
            
            field :video do
                label 'Youtube'
            end
            
           
            field :preview, :ck_editor
           
            
            field :detail, :ck_editor
            
            
            field :description do
              label 'Описание '
            end
            
            field :keywords do
              label 'Ключевые слова '
            end
            
        end 
        
         
    end
    
    
end
