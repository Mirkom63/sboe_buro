$(document).ready(function(){



    $(".info_team_detail").swipe( {
        //Generic swipe handler for all directions
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {

            if(direction=='left'){
                $(this).animate({'left':'-=400px'},500);
                setTimeout(function(){
                   $('.info_team_detail').css('left','-15px').hide();
                },1000);
            }

            if(direction=='right'){
                $(this).animate({'left':'+=800px'},500);
                setTimeout(function(){
                   $('.info_team_detail').css('left','-15px').hide();
                },1000);
            }
        }
      });

    if($('.mobile_site_object').val()=='yes'){


        var elem = document.getElementById('mySwipe');
        window.mySwipe = Swipe(elem, {
          // startSlide: 4,
          // auto: 3000,
          // continuous: true,
          // disableScroll: true,
          // stopPropagation: true,
          // callback: function(index, element) {},
          // transitionEnd: function(index, element) {}
        });


        var id_detail_page=$('.js-load-detail-page').val();


        if(id_detail_page.length!=0){
            mySwipe.next();

            $('.scroll_box_object_detail_mobile').html('');
            $('body').animate({ scrollTop: 0}, 800);
            var id=$(this).attr('id');
            $.ajax({
                type     :'POST',
                cache    : false,
                data: 'id='+id_detail_page,
                url  : '/detail_mobile',
                success  : function(response) {
                    $('.scroll_box_object_detail_mobile').html(response);
                }
            });


        }

        $('.js-button-to-up-object').live('click',function(){
            $('.scroll_box_object_detail_mobile').animate({ scrollTop: 0}, 800);
        });

        $(window).resize(function(){
            if(document.all){
                var width = document.body.clientWidth;
                var height = document.body.clientHeight;
            }else{
                var width = innerWidth;
                var height = innerHeight;
            }
            $('.scroll_box_object_detail_mobile').height(height);
        });
        if(document.all){
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
        }else{
            var width = innerWidth;
            var height = innerHeight;
        }

        $('.scroll_box_object_detail_mobile').height(height);

        $('.js-button-back-object').live('click',function(){
            $('.scroll_box_object_detail_mobile').html('');
        });

        $('.point_object_mobile').click(function(){
            $('.scroll_box_object_detail_mobile').html('');
            $('body').animate({ scrollTop: 0}, 800);
            var id=$(this).attr('id');

            window.history.pushState(null, null, '/object/'+id);

            $.ajax({
                type     :'POST',
                cache    : false,
                data: 'id='+id,
                url  : '/detail_mobile',
                success  : function(response) {
                    $('.scroll_box_object_detail_mobile').html(response);
                }
            });


        });

    }
    if($('.mobile_site_main').val()=='yes'){
        $('.back_white_contact_us').click(function(){
            $(this).fadeOut();
        });
        $('.main_logo_mobile').click(function(){
            $('.back_white_contact_us').fadeIn();
        });
        $('.main_logo').click(function(){
            if($('.main_logo').is('.main_logo_mobile')){

            }
        });

        $('.js-open-logo').mousedown(function(){
            set_cookie('open_logo','yes','/');
            $('.main_logo').addClass('main_logo_mobile');
            $('.main_block_mobile').fadeIn();
        });

        function load_resize(){

            if(document.all){
                var width = document.body.clientWidth;
                var height = document.body.clientHeight;
            }else{
                var width = innerWidth;
                var height = innerHeight;
            }
            $('.block_mobile').height(height);
        }
        load_resize();
        $(window).resize(function(){
            load_resize();
        });




    }

});


function set_cookie( name, value, path,expires, domain, secure )
{
// set time, it's in milliseconds
var today = new Date();
today.setTime( today.getTime() );
path='/';
/*
if the expires variable is set, make the correct
expires time, the current script below will set
it for x number of days, to make it for hours,
delete * 24, for minutes, delete * 60 * 24
*/
if ( expires )
{
expires = expires * 1000 * 60 * 60 * 24;
}
var expires_date = new Date( today.getTime() + (expires) );

document.cookie = name + "=" +escape( value ) +
( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
( ( path ) ? ";path=" + path : "" ) +
( ( domain ) ? ";domain=" + domain : "" ) +
( ( secure ) ? ";secure" : "" );
}

function delete_cookie ( cookie_name )
{
  var cookie_date = new Date ( );  // Текущая дата и время
  cookie_date.setTime ( cookie_date.getTime() - 1 );
  document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}

function get_cookie ( cookie_name )
{
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}
