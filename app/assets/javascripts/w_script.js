$(document).ready(function(){
    
    if(device.mobile()){
        $('head').append('<meta name="viewport" content="width=330px,user-scalable=no">');
    }
    if(device.tablet()){
        $('head').append('<meta name="viewport" content="width=800px,user-scalable=no">');
    }
    if(device.desktop()){
        $('head').append('<meta name="viewport" content="width=960px,user-scalable=no">');
    }
    
    
    
    
    if($('.js-show-art').val()=='yes'){
        if(document.all){
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
        }else{
            var width = innerWidth;
            var height = innerHeight;
        } 
        $('.point_art_slide').height(height);
        $('.main_art').height(height);
        
        $('.box_ugol_art').click(function(){
            $('body').animate({ scrollTop: height}, 800);
        }); 
        $('.scroll_to_up').live('click',function(){
            $('body,html').animate({ scrollTop: 0}, 800);
        });

        $('.load_more').click(function(){
            var id=$(this).attr('id');
            $('.load_icon_art').show();
            $('.text_load_art').hide();
            $.ajax({
                type     :'POST', 
                cache    : false,
                data: 'id='+id,
                url  : '/load_art',
                success  : function(response) {
                    $('.load_icon_art').hide();
                    $('.text_load_art').show();
                    id++;
                    $('.load_more').attr('id',id);
                    var loader=$('.loader_new_art');
                    $('.loader_new_art').html(response).removeClass('loader_new_art');
                    loader.find('.point_art_slide').height(height);
                    
                    $.ajax({
                        type     :'POST', 
                        cache    : false,
                        data: 'id='+id,
                        url  : '/load_art',
                        success  : function(response) {
                            response=response.replace(/[\s{2,}]+/g, '');
                            if(response=='<divclass="loader_new_art"></div>'){
                                $('.load_more').hide();
                            }
                           
                        }
                    });
                }
            });
        });
        
        /*
        $(window).scroll(function(){
            
            if  ($(window).scrollTop() == $(document).height() - $(window).height()) {
                
                var id=parseInt($('.art_block').attr('id'));
                id=id-1; 
                
                $.ajax({
                    type     :'POST', 
                    cache    : false,
                    data: 'id='+id,
                    url  : '/load_art',
                    success  : function(response) {
                        $('.loader_new_art').html(response).removeClass('loader_new_art');
                        
                        id=$('.point_art_slide').last().attr('id');
                        
                        $('.loader_new_art').find('.point_art_slide').attr('id')
                        
                        $('.art_block').attr('id',id);
                        
                        $('.point_art_slide').height(height);
                    }
                });
            }
        });
        */
        

    }
    
    
    
    /*
    $('.main_block').mouseover(function(){
        $('.js-open-main-contact').css('color','#000');
    });
    $('.main_block').mouseout(function(){
        $('.js-open-main-contact').css('color','#fff');
    });
    */
    
    $('input').keyup(function(){
        $(this).css('background','#FFF');
    });
    $('textarea').keyup(function(){
        $(this).css('background','#FFF');
    });
    
    $('.button_send').click(function(){
        var name=$('input[name="name"]').val();
        var phone=$('input[name="phone"]').val();
        var email=$('input[name="email"]').val();
        var message=$('textarea[name="message"]').val();
        var send_form=true;
        if(name.length==0){
            send_form=false;
            $('input[name="name"]').css('background','tomato');
        }
        if(phone.length==0){
            send_form=false;
            $('input[name="phone"]').css('background','tomato');
        }
        if(email.length==0){
            send_form=false;
            $('input[name="email"]').css('background','tomato');
        }
        if(message.length==0){
            send_form=false;
            $('textarea[name="message"]').css('background','tomato');
        }
        
        if(send_form==true){
            $.ajax({
                type     :'POST', 
                cache    : false,
                data: $('.form_contact').serialize(),
                url  : '/send_message',
                success  : function(response) {
                    $('.form_contact').html('<div class="thanks">Спасибо! <br/> Заявка принята!</div>');
                }
            });
        }
        
        
    });
    
    
    $('.close_info_team').click(function(){
        $('.info_team_detail').fadeOut(); 
    });
   
    $('.point_team').click(function(){
        if($(this).find('.info_team_detail').css('display')=='none'){
            $(this).find('.info_team_detail').fadeIn();
        }else{
            $('.info_team_detail').fadeOut();
        }
    });
    
    
    function load_detail(coord){
        $.ajax({
            type     :'POST', 
            cache    : false,
            data: 'coord='+coord,
            url  : '/detail',
            success  : function(response) { 
                $('.block_load_detail').html(response);
                var load_detail=$('.load_detail').val();
                window.history.pushState(null, null, '/object/'+load_detail+'/');
                $('.slider_detail').owlCarousel({
                    navigation : true,
                    slideSpeed : 800,
                    paginationSpeed : 400,
                    singleItem : true,
                    pagination:false,
                });
                $('.detail_block').fadeIn();
            }
        });

        
        ymaps.ready(init_detail);
        var myMap,
        myPlacemark;

        function init_detail(){
          
            var myMap = new ymaps.Map('map_list', {
              center: [coord[0],coord[1]],
              zoom: 13,
            });
            
            var myCollection = new ymaps.GeoObjectCollection();

            myPlacemark1 = new ymaps.Placemark([coord[0],coord[1]],{},{
                iconImageHref: '/img/default_marker.png',
                iconImageSize: [27, 35],
                iconImageOffset: [-23.5, -35]
            });

            myMap.geoObjects.add(myPlacemark1); 

        }
    }
    
    
    $('.gallery_detail').fancybox({

        arrows    : true,
        nextClick : true,
        padding: 'none',
        helpers : {
            thumbs : {
                width  : 50,
                height : 60
            }
        }
    });


    ymaps.ready(init);
        var myMap,
        myPlacemark;

    function init(){

        var myMap = new ymaps.Map('big_map', {
          center: [53.921742799801946,54.056958290079926],
          zoom: 4,
        });

        myMap.controls
        .add('zoomControl', { left: 5, bottom: 35 })


        $('.point_work').each(function(){


            var coord=$(this).attr('coord');
            var marker=$(this).attr('marker');
            var name=$(this).val();
            coord=coord.split(',');
            if(marker.length==0){
                myPlacemark = new ymaps.Placemark([coord[0],coord[1]]);
            }else{
                myPlacemark = new ymaps.Placemark([coord[0],coord[1]],{},{
                    iconImageHref: '/img/default_marker.png',
                    iconImageSize: [27, 35],
                    iconImageOffset: [-23.5, -35]
                });
            }


            myMap.geoObjects.add(myPlacemark); 

            myPlacemark.events.add('click', function (event) {
                var coords = event.get("target").geometry.getCoordinates();
                load_detail(coord);
            });



        });

    }


    if($('.js-load_detail').val()=='yes'){
       
        
        $('.slider_detail').owlCarousel({
            navigation : true,
            slideSpeed : 800,
            paginationSpeed : 400,
            singleItem : true,
            pagination:false,
        });  
        
        var coord=$('.js-coord-detail').val().split(',');
        
        ymaps.ready(init_detail);
        var myMap,
        myPlacemark;

        function init_detail(){
          
            var myMap = new ymaps.Map('map_list', {
              center: [coord[0],coord[1]],
              zoom: 13,
            });
            
            var myCollection = new ymaps.GeoObjectCollection();

            myPlacemark1 = new ymaps.Placemark([coord[0],coord[1]],{},{
                iconImageHref: '/img/default_marker.png',
                iconImageSize: [27, 35],
                iconImageOffset: [-23.5, -35]
            });
            
             
            
            myMap.geoObjects.add(myPlacemark1); 

        }
        
    }
    
    $('.gallery').fancybox({
        arrows    : true,
        nextClick : true,
        padding: 'none',
        helpers : {
            thumbs : {
                width  : 50,
                height : 60
            }
        }
    });
    
    
    $('.point_object').click(function(){
        $('.point_object').removeClass('active_object');
        $(this).addClass('active_object');
        $('#map_list').html('');
        var coord=$(this).attr('coord');
        coord=coord.split(',');
        
        load_detail(coord);

    });
    
    $('.js-open-main-contact').click(function(){
        $('.box_contact_main').fadeIn();
    });
    
    $('.js-close-main-contact').click(function(){
        $('.box_contact_main').fadeOut();
    });
    
    $('.box_object').mCustomScrollbar(); 
    
    /*
    $(document).mouseup(function (e){ // событие клика по веб-документу
		var div = $(".open_youtube").find('iframe'); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
		    && div.has(e.target).length === 0) { // и не по его дочерним элементам
			$('.open_youtube').fadeOut(); // скрываем его
            $('.open_youtube').find('iframe').attr('src','');
		}
	});
    
    
    
    $('.gallery').fancybox({
        //prevEffect : 'none',
        //nextEffect : 'none',
        closeBtn  : false,
        //arrows    : false,
        nextClick : true,
        padding: 'none',
        helpers : {
            thumbs : {
                width  : 50,
                height : 60
            }
        }
    });
    
    
    var top_team=0;
    var left_team=0;
    $('.close_team').live('click',function(){
        $('.load_block').find('div').fadeOut();
        $('.load_block').animate({'width':'240px','height':'240px','left':left_team+'px','top':top_team+'px','border-radius':'50%'},300);
        setTimeout(function(){
            $('.load_block').hide().html('');
        },300);
    });
    
    /*
    $('.point_team').live('click',function(){
        var id=$(this).find('.avatar').attr('id');
        var src=$(this).find('.avatar').attr('src');
        var pos=document.getElementById(id).getBoundingClientRect();
        top_team=pos.top;
        left_team=pos.left;
        $('.load_block').css({'top':pos.top+'px','left':pos.left+'px','background-image':'url('+src+')'}).fadeIn(100);
        setTimeout(function(){
            $('.load_block').animate({'width':'100%','height':'100%','left':'0px','top':'0px','border-radius':'0px'},300);
            setTimeout(function(){
                $('.load_block').html('<div class="descr_team">Описание</div><div class="close_detail close_contact close_team js-close-team"></div>');
            });
        },300);
    });

    
    $('.bottom_info').click(function(){
        if($('.contact_main').css('display')=='none'){
            $('.contact_main').fadeIn();
        }else{
            $('.contact_main').fadeOut();
        }
    });
    
    $('.box_youtube').live('click',function(){
        var id=$(this).attr('id');
        $('.open_youtube').find('iframe').attr('src','https://www.youtube.com/embed/'+id);
        $('.open_youtube').fadeIn();
    });
    $('.js-close-video').live('click',function(){
        $('.open_youtube').fadeOut();
        $('.open_youtube').find('iframe').attr('src','');
    });
    
    var open_page=false;
    var load_page=$('.load_page').val();
    
    if(load_page.length!=0){
        open_page=true;
        load_page_ajax($('.js-page-'+load_page));
        
    }
    
    $('.js-menu-close-block').live('click',function(){
        var area=$(this).parent().parent().parent().parent().parent();
        area.fadeOut();
        setTimeout(function(){
            $('.table_main_block').fadeIn();
            area.find('.block_load_content').html(' ');
            area.parent().removeClass('condition_open_page');
            open_page=false;
            window.history.pushState(null, null, '/');
            $('.load_detail').val('');
        },500);
    });
    
    $('.js-logo-close-block').live('click',function(){
        var area=$(this).parent().parent().parent().parent();
        area.fadeOut();
        setTimeout(function(){
            $('.table_main_block').fadeIn();
            area.find('.block_load_content').html(' ');
            area.parent().removeClass('condition_open_page');
            open_page=false;
            window.history.pushState(null, null, '/');
            $('.load_detail').val('');
        },500);
    });
    
    $('.js-close-block').live('click',function(){
        var area=$(this).parent().parent().parent().parent();
        area.fadeOut();
        setTimeout(function(){
            $('.table_main_block').fadeIn();
            area.find('.block_load_content').html(' ');
            area.parent().removeClass('condition_open_page');
            open_page=false;
            window.history.pushState(null, null, '/');
        },500);
    });
    $('.js-open-page').live('click',function(){
        if(open_page==false){
            open_page=true;
            load_page_ajax($(this));
        }
    });
     
    function load_page_ajax(obj){
        
        obj.addClass('condition_open_page');
        var page=obj.attr('page');
        var area=obj.find('.block_load_content');
        $.ajax({
            type     :'GET', 
            cache    : false,
            data: false,
            url  : '/load_page/'+page,
            success  : function(response) {
                
                area.html(response);
                setTimeout(function(){
                    area.fadeIn();
                    var load_detail=$('.load_detail').val();
                    if(load_detail.length!=0){
                        window.history.pushState(null, null, '/'+page+'/'+load_detail+'/');
                    }else{
                        window.history.pushState(null, null, '/'+page+'/');
                    }
                    
                   
                    
                    if(page=='team'){
         
                        function avatar(obj,slide){
                            
                            var left=230*obj;
                            var top=230*slide;
                            obj++;
                            $('#ava_'+obj).css('background-position','-'+left+'px -'+top+'px');
                            var number=$('#ava_'+obj).data('number');
                            setTimeout(function(){
                                slide++;
                                obj--;
                                if(number!=slide){
                                    avatar(obj,slide);
                                }else{
                                    setTimeout(function(){
                                        obj++;
                                        if(obj==3){
                                            obj=0;
                                        }
                                        avatar(obj,0);
                                    },1000);
                                }
                                
                            },700);
                        }
                        
                        avatar(0,0);


                        ymaps.ready(init_2);
                            var myMap,
                            myPlacemark;

                        function init_2(){

                            var myMap = new ymaps.Map('map_contact', {
                              center: [55.75161665120036,37.61788699999992],
                              zoom: 8,
                            });

                            myMap.controls
                            .add('zoomControl', { left: 20, top: 10 })
                            .add('typeSelector',{ right: 20, top: 20 })


                            var myCollection = new ymaps.GeoObjectCollection();

                            myPlacemark1 = new ymaps.Placemark([55.75161665120036,37.61788699999992]);

                            myMap.geoObjects.add(myPlacemark1); 


                        }
                    }
                    
                    if(page=='catalog'){
                        
                        $('.box_object').mCustomScrollbar(); 
                        
                        var load_detail=$('.load_detail').val();
                        
                        if(load_detail.length!=0){

                            $.ajax({
                                type     :'POST', 
                                cache    : false,
                                data: 'id='+load_detail,
                                url  : '/return_coord',
                                success  : function(response) {
                                    open_detail(response);
                                }
                            });
                        }
                            
                        

                        
                    }
                    
                    
                    ymaps.ready(init);
                            var myMap,
                            myPlacemark;

                        function init(){

                            var myMap = new ymaps.Map('big_map', {
                              center: [56.821742799801946,49.056958290079926],
                              zoom: 4,
                            });

                            myMap.controls
                            .add('zoomControl', { left: 290, top: 130 })
                            .add('typeSelector',{ right: 20, top: 80 })

                            $('.point_work').each(function(){
                                
                                
                                var coord=$(this).attr('coord');
                                var marker=$(this).attr('marker');
                                var name=$(this).val();
                                coord=coord.split(',');
                                if(marker.length==0){
                                    myPlacemark = new ymaps.Placemark([coord[0],coord[1]]);
                                }else{
                                    myPlacemark = new ymaps.Placemark([coord[0],coord[1]],{},{
                                        iconImageHref: '/img/default_marker.png',
                                        iconImageSize: [38, 50],
                                        iconImageOffset: [-19, -50]
                                    });
                                }
                                
                                
                                myMap.geoObjects.add(myPlacemark); 

                                myPlacemark.events.add('click', function (event) {
                                    var coords = event.get("target").geometry.getCoordinates();
                                    open_detail(coords[0]+','+coords[1]);
                                }); 

                            }); 

                        }
                    
                    
                },1000);

            }
        });
    }
    


    
    
    $('.point_object').live('click',function(){
      
        if(document.all){
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
        }else{
            var width = innerWidth;
            var height = innerHeight;
        } 
        
        $('#map_list').html('');
      
        open_detail($(this).attr('coord'));
        
    });
    
    
    
    
    $('.close_detail').live('click',function(){
        if(document.all){
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
        }else{
            var width = innerWidth;
            var height = innerHeight;
        }  
        $('.detail_box').css('top','-'+height+'px');
        $('#map_list').css('bottom','-200px');
        $('#big_map').fadeIn(); 
        setTimeout(function(){
            $('#map_list').html('');
        },300);
        window.history.pushState(null, null, '/catalog/');
        $('.load_detail').val('');
    });
    
    
    function open_detail(coord){
       
        
            var url_marker='';
            var data=new Array;
        
      
            $.ajax({
                type     :'POST', 
                cache    : false,
                data: 'coord='+coord,
                url  : '/return_id',
                success  : function(response) {
                  
                    data=response.split('|');
                    window.history.pushState(null, null, '/catalog/'+data[0]);
                    
                    $.ajax({
                        type     :'POST', 
                        cache    : false,
                        data: 'coord='+coord,
                        url  : '/detail',
                        success  : function(response) {
                            
                          
                            coord=coord.split(',');
                            $('.detail_box').html(response);
                            $('.slider_detail').owlCarousel({
                                navigation : true,
                                slideSpeed : 800,
                                paginationSpeed : 400,
                                singleItem : true,
                                pagination:false,
                            });

                            ymaps.ready(init);
                            var myMap,
                            myPlacemark;

                            function init(){

                                var myMap = new ymaps.Map('map_list', {
                                  center: [coord[0],coord[1]],
                                  zoom: 13,
                                });

                                myMap.controls
                                .add('zoomControl', { left: 20, top: 10 })
                                .add('typeSelector',{ right: 20, top: 20 })


                                var myCollection = new ymaps.GeoObjectCollection();

                                
                                
                                if(data[1].length==0){
                                    myPlacemark1 = new ymaps.Placemark([coord[0],coord[1]]);
                                }else{
                                    myPlacemark1 = new ymaps.Placemark([coord[0],coord[1]],{},{
                                        iconImageHref: '/img/default_marker.png',
                                        iconImageSize: [38, 50],
                                        iconImageOffset: [-19, -50]
                                    });
                                }

                                


                                $('.detail_box').css('top','0px');
                                $('#map_list').css('bottom','0px'); 
                                $('#big_map').fadeOut(); 


                                myMap.geoObjects.add(myPlacemark1); 

                            }
                            
                             $('.gallery_detail').fancybox({
                                //prevEffect : 'none',
                                //nextEffect : 'none',
                                closeBtn  : true,
                                //arrows    : false,
                                nextClick : true,
                                padding: 'none',
                                helpers : {
                                    thumbs : {
                                        width  : 50,
                                        height : 60
                                    }
                                }
                            });
                            
                            

                        }
                    });


                }
            });
        

    }
    
   
    
    function load_ugol(){
        if(document.all){
            var width = document.body.clientWidth;
            var height = document.body.clientHeight;
        }else{
            var width = innerWidth;
            var height = innerHeight;
        } 
        $('.slide_art').height(height);
    }
    load_ugol();
    
    $(window).resize(function(){
        load_ugol();
    });
    
    */
    
    
    
});
