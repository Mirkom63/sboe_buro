Rails.application.routes.draw do
    
  
 
    
    mount Ckeditor::Engine => '/ckeditor'
    devise_for :users

    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
    
    root 'site#index'

    post 'load_art' => 'site#load_art'
    get 'load_art' => 'site#load_art'
    post 'add_comment' => 'site#add_comment'
    
    post '/send_message'  => 'site#send_message'
    post '/detail'  => 'site#detail'
    post '/detail_mobile'  => 'site#detail_mobile'
    post 'return_id' => 'site#return_id'
    post 'return_coord' => 'site#return_coord'

    
   
    get '/:pattern' => 'site#index'
    get '/:pattern/:id' => 'site#index'
  
    
    
    
    
    resources :site

end
